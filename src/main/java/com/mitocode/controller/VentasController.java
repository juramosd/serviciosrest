package com.mitocode.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.service.IVentaService;
import com.mitocode.exception.ModelNotFoundException;
import com.mitocode.model.Venta;

@RestController
@RequestMapping("/ventas")
public class VentasController {
	@Autowired
	private IVentaService service;
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Venta>> listar(){
		List<Venta> venta = new ArrayList<>();
		venta = service.listar();
		return new ResponseEntity<List<Venta>>(venta,HttpStatus.OK);			
	}
	@GetMapping(value= "/{id}", produces = "application/json")
	public Resource<Venta> listarPorId(@PathVariable("id") Integer id){
		Venta pac = service.listarId(id);
		if(pac == null) {
			throw new ModelNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		Resource<Venta> resource = new Resource<Venta>(pac);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));
		
		resource.add(linkTo.withRel("venta-resource"));
		
		return resource;			
	}
	
	@PostMapping(produces= "application/json", consumes = "application/json")	
	public ResponseEntity<Object> registrar(@RequestBody Venta venta){
		Venta pac = new Venta();
		pac = service.registrar(venta);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pac.getIdVenta()).toUri();
		return ResponseEntity.created(location).build();		
	}
	
	@PutMapping(produces= "application/json", consumes = "application/json")	
	public ResponseEntity<Object> modificar(@RequestBody Venta venta){
		service.modificar(venta);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")	
	public void eliminar(@PathVariable("id") Integer id){
		Venta pac = service.listarId(id);
		if(pac == null) {
			throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
		}else {
			service.eliminar(id);		
		}		
	}
}
