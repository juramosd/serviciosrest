package com.mitocode.dao;

import com.mitocode.model.Persona;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonaDAO extends JpaRepository<Persona,Integer>{

}
