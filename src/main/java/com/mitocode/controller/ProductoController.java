package com.mitocode.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModelNotFoundException;
import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {
	
	@Autowired
	private IProductoService service;
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Producto>> listar(){
		List<Producto> productos = new ArrayList<>();
		productos = service.listar();
		
		return new ResponseEntity<List<Producto>>(productos,HttpStatus.OK);
	}
	@GetMapping(value= "/{id}", produces = "application/json")
	public Resource<Producto> listarPorId(@PathVariable("id") Integer id){
		Producto pro = service.listarId(id);
		if(pro == null) {
			throw new ModelNotFoundException("ID NO ENCONTRADO: "+id);
		}
		Resource<Producto> resource = new Resource<Producto>(pro);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));
		
		resource.add(linkTo.withRel("producto-resource"));
		return resource;
	}
	
	@PostMapping(produces= "application/json", consumes = "application/json")	
	public ResponseEntity<Object> registrar(@RequestBody Producto producto){
		Producto pro = new Producto();
		pro = service.registrar(producto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pro.getIdProducto()).toUri();
		
		return ResponseEntity.created(location).build();		
	}
	
	@PutMapping(produces= "application/json", consumes = "application/json")	
	public ResponseEntity<Object> modificar(@RequestBody Producto producto){
		service.modificar(producto);
		
		return new ResponseEntity<Object>(HttpStatus.OK);		
	}
	
	@DeleteMapping(value = "/{id}")	
	public void eliminar(@PathVariable("id") Integer id){
		Producto pac = service.listarId(id);
		if(pac == null) {
			throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
		}else {
			service.eliminar(id);		
		}	
	}	
}
